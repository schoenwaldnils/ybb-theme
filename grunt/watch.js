module.exports = {
  css: {
    tasks: [
      'suitcss',
      'scsslint'
    ],
    files: [
      './components_local/**/*.css',
      './assets/stylesheets/**/*.css'
    ]
  },
  js: {
    tasks: ['componentbuild'],
    files: [
      './index.js',
      './assets/scripts/**/*.js',
      './components_local/**/*.js'
    ]
  },
  webfont: {
    tasks: ['webfont'],
    files: [
      './assets/images/icons/**/*.svg'
    ]
  }
};
