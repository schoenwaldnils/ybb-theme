module.exports = {
  dist: {
    options: {
      name: 'index',
      install: true,
      verbose: true,
      copy: true,
      require: true
    },
    src: './',
    dest: './build/scripts'
  }
};
