module.exports = {
  dist: {
    files: {
      'style.css': ['component.json']
    },
    options: {
      resolveOpts: {
        install: true,
        out: 'components',
      }
    }
  }
};
