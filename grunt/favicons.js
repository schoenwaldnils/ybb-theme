module.exports = {
  options: {
    trueColor: true,
    precomposed: false,
    coast: false,
    appleTouchBackgroundColor: 'auto',
    tileBlackWhite: false,
    tileColor: 'none',
    html: 'favicons.html',
    HTMLPrefix: '/wp-content/themes/ybb-theme/build/images/favicons/',
  },
  icons: {
    src: 'assets/images/logo/favicon.png',
    dest: 'build/images/favicons'
  }
};
