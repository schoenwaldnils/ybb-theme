module.exports = {
  symbol: {
    cwd: 'assets/images/svg',
    src: ['**/*.svg'],
    dest: 'build/images/svg',
    options: {
      mode: {
        symbol: {
          render: {
            css: true
          },
          example: true
        }
      }
    }
  }
};
