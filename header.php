<!DOCTYPE html>
<html>

<head>
  <meta http-equiv="content-type" content="text/html; charset=<?php bloginfo( 'charset' ); ?>" />
  <title>
    <?php bloginfo( 'name' ); ?>
    <?php wp_title(); ?>
  </title>
  <?php wp_head(); ?>

  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <link rel='pingback' href='<?php bloginfo( ' pingback_url ' ); ?>' />
</head>

<body <?php body_class(); ?>>
  <div class="Area Area--meta">
    <div class="Area-content">
      <?php print component('meta'); ?>
    </div>
  </div>
  <header class="Area Area--header">
    <div class="Area-content">
      <?php print component('header'); ?>
    </div>
  </header>
