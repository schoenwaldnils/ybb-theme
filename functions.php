<?php

/*
 * Global theme urls
 */
$url_image = get_template_directory_uri() . '/assets/images/';
$url_svg = $url_image . 'svg/';

set_query_var( 'url_image', $url_image );
set_query_var( 'url_svg', $url_svg );


/*
 * Enque CSS an JS
 */

function ybb_enqueue_scripts() {
// register CSS
  wp_enqueue_style( 'ybb-css', get_template_directory_uri() . '/style.css' );
  wp_enqueue_style( 'ybb-gfont', 'http://fonts.googleapis.com/css?family=Open+Sans:400,600,700' );

// register JS
  wp_enqueue_script( 'jquery' );
  wp_enqueue_script( 'ybb-js', get_template_directory_uri() . '/build/scripts/index.js',
    array(
      'jquery',
      'jquery-ui-core',
      'jquery-ui-widget' ) );
}

add_action( 'wp_enqueue_scripts', 'ybb_enqueue_scripts' );


/*
 * Register widget areas
 */
if ( function_exists('register_sidebar') ) {
  register_sidebar(array(
    'name'          => 'Footer left',
    'id'            => 'footer-left',
    'before_widget' => '<div class="Footer-widget Widget">',
    'before_title'  => '<p class="Widget-title u-fontGamma">',
    'after_title'   => '</p>',
    'after_widget'  => '</div>',
  ));
  register_sidebar(array(
    'name'          => 'Footer right',
    'id'            => 'footer-right',
    'before_widget' => '<div class="Footer-widget Widget">',
    'before_title'  => '<p class="Widget-title u-fontGamma">',
    'after_title'   => '</p>',
    'after_widget'  => '</div>',
  ));
  register_sidebar(array(
    'name'          => 'Footer bottom',
    'id'            => 'footer-bottom',
    'before_widget' => '<div class="Footer-widget Widget">',
    'before_title'  => '<p class="Widget-title u-fontGamma">',
    'after_title'   => '</p>',
    'after_widget'  => '</div>',
  ));
}

/*
 * Register menus
 */
if ( function_exists( 'register_nav_menus' ) ) {
  register_nav_menus(
    array(
      'header_menu' => 'Header Menu',
      'meta_menu' => 'Meta Menu'
    )
  );
}


class mainmanu_walker extends Walker_Nav_Menu {
  function start_el(&$output, $item, $depth, $args) {
    global $wp_query;
    $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

    $class_names = $value = '';

    $classes = empty( $item->classes ) ? array() : (array) $item->classes;

    $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) );
    $class_names = ' class="'. esc_attr( $class_names ) . '"';

    $output .= $indent . '<li id="Menu-item-'. $item->ID . '"' . $value . $class_names .'>';

    $attributes = ' class="Menu-link"';
    $attributes .= ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
    $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
    $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
    $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';

    $prepend = '<div class="menu-text">';
    $append = '</div>';
    $description  = ! empty( $item->description ) ? '<span>'.esc_attr( $item->description ).'</span>' : '';

    if($depth != 0) {
      $description = $append = $prepend = "";
    }

    $item_output = $args->before;
    $item_output .= '<a'. $attributes .'>';
    $item_output .= $args->link_before .$prepend.apply_filters( 'the_title', $item->title, $item->ID ).$append;
    $item_output .= $description.$args->link_after;
    $item_output .= '</a>';
    $item_output .= $args->after;

    $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
  }
}

add_theme_support( 'post-thumbnails' );

if ( function_exists( 'add_image_size' ) ) {
    add_image_size( 'banner', 3000, 9999 );
    add_image_size( 'wide-banner', 9999, 300, true );
}

require_once('includes/components.php');
require_once('includes/custom_post_types.php');

?>
