<?php get_header(); ?>
  <div class="Area Area--banner">
    <div class="Banner Banner--slider">
      <?php layerslider('frontslider') ?>
    </div>
  </div>

  <?php if (get_the_content()) { ?>
    <div class="Area Area--main">
      <div class="Area-content">
        <?php the_content(); ?>
      </div>
    </div>
  <?php } ?>

  <div class="Area Area--main">
    <div class="Area-content">
      <h2 class="Block-title u-fontAlpha u-textCenter u-marginBottom--large">
        <?php the_block( 'White Headline', array('type' => 'one-liner')); ?>
      </h2>
      <div class="Grid Grid--withGutter">
        <div class="Grid-cell u-sm-size1of2 u-md-size1of4">
          <div class="u-marginBottom--medium">
            <?php the_block( 'White 1'); ?>
          </div>
        </div>
        <div class="Grid-cell u-sm-size1of2 u-md-size1of4">
          <div class="u-marginBottom--medium">
            <?php the_block( 'White 2'); ?>
          </div>
        </div>
        <div class="Grid-cell u-sm-size1of2 u-md-size1of4">
          <div class="u-marginBottom--medium">
            <?php the_block( 'White 3'); ?>
          </div>
        </div>
        <div class="Grid-cell u-sm-size1of2 u-md-size1of4">
          <div class="u-marginBottom--medium">
            <?php the_block( 'White 4'); ?>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="Area Area--main Area--purple">
    <div class="Area-content">
      <h2 class="Block-title u-fontAlpha u-textCenter u-marginBottom--large">
        <?php the_block( "Lila-Headline", array('type' => 'one-liner')); ?>
      </h2>
      <div class="Block-quote u-textCenter">
        <?php //the_block( 'Lila-text'); ?>
         <div class="FlexSlider js-FlexSlider"cp>
          <ul class="FlexSlider-slides">
          <?php
            $args = array(
              'post_type' => 'testemonial',
              'orderby' => 'rand'
            );
            query_posts( $args );

            // The Loop
            while ( have_posts() ) : the_post(); ?>
              <li class="FlexSlider-slide">
                <div class="FlexSlider-slide">
                  <?php the_post_thumbnail('post-thumbnail', array( 'class' => 'Block-image l-imageRound u-marginBottom--small aligncenter' )); ?>
                  <div class="Block-text u-marginBottom--small">
                    <?php the_content() ?>
                  </div>
                  <?php the_title(); ?>
                  <!-- , Kunde seit <?php the_field('customer-since'); ?> -->
                </div>
              </li>
            <?php
            endwhile;

            // Reset Query
            wp_reset_query();
            ?>

          </ul>
        </div>
      </div>
    </div>
  </div>

  <?php print component('partner-and-seal') ?>

<?php get_footer(); ?>
