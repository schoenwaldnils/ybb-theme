<?php

/*
 * Components
 */

/*
 * Register components
 */

// function register_components() {

//   $theme_dir = 'wp-content/themes/' . wp_get_theme() . '/';
//   $base = 'components_local/';
//   $components_dir = $theme_dir . $base;

//   $components = glob($components_dir . '**/component.json');

//   foreach ($components as $component) {
//     $json = json_decode(file_get_contents($component), TRUE);
//     $name = $json['name'];

//     $component_function = 'sc_' . $name;
//     $$component_function = function ($args = NULL) {
//       if (isset($json['backend']['arguments'])) {
//         $args = shortcode_atts($json['backend']['arguments'], $args);
//       }
//     };

//     add_shortcode( $name, $$component_function );
//   }

//   $json = json_decode(file_get_contents($components[4]), TRUE);
//   $name = $json['name'];

//   function sc_header($args = NULL) {
//     if (isset($json['backend']['arguments'])) {
//       $args = shortcode_atts($json['backend']['arguments'], $args);
//     }
//     print $name;
//     $tpl_name = str_replace('.php', '',$json['backend']['template']);
//     $component_tpl = $base . $name . '/' . $tpl_name;
//     get_template_part($component_tpl, true);
//   };

//   add_shortcode( 'cp_header', 'sc_header' );

// }

// register_components();


function find_components() {

  $theme_dir = 'wp-content/themes/' . wp_get_theme() . '/';

  if (file_exists($theme_dir . 'component.json')) {
    $root_json = json_decode(file_get_contents($theme_dir . 'component.json'), TRUE);

    if ($root_json['locals']) {
      $components['locals'] = $root_json['locals'];
    }

    if ($root_json['dependencies']) {
      $components['dependencies'] = $root_json['dependencies'];
    }

    return $components;
  }

};

function check_component_version($name) {

  $components = find_components();

  if (in_array($name, $components['dependencies'])) {
    print('in-array');
    if ($components['dependencies'][$name] !== '*') {
      print_r($components['dependencies'][$name]);
      return $components['dependencies'][$name];
    }
    else {
      return null;
    }
  }

}

function check_if_component_exist($name) {

}

function component($name, $args = NULL) {

  if (strpos($name,'/') !== false) {
    list($repo_user, $name) = explode('/', $name);

    print_r($repo_user);
    print_r($name);
    $version = check_component_version($name);
    print_r($version);
  } else {
    $is_local = true;
  }



  $theme_dir = 'wp-content/themes/' . wp_get_theme() . '/';

  if ($is_local) {
    $base = 'components_local/';
    $folder = '/';
  } else {
    $base = 'components/';
    if ($version) {
      $folder = $version;
    } else {
      $folder = '/*/';
    }
  }

  $component_dir = $theme_dir . $base . $name . $folder;


  if (file_exists($component_dir . 'component.json')) {

    $json = json_decode(file_get_contents($component_dir . 'component.json'), TRUE);

    if (isset($json['backend']['arguments'])) {
      $args = shortcode_atts($json['backend']['arguments'], $args);
    } else {
      $args = shortcode_atts($json['backend']['fixture'], $args);
      $args = shortcode_atts($json['backend']['options'], $args);
    }

    foreach ($args as $key => $value) {
      set_query_var( $key, $value );
    }

    /* theme relevant vars */
    global $url_image;
    global $url_svg;
    set_query_var( 'url_image', $url_image );
    set_query_var( 'url_svg', $url_svg );

    $tpl_name = str_replace('.php', '', $json['backend']['template']);

    $component_tpl = $base . $name . $folder . $tpl_name;
    get_template_part($component_tpl, true);

  } else {
    return "No component found";
  }
}

?>
