<?php

// Register Custom Post Type
function testemonial() {

  $labels = array(
    'name'                => 'Meinungen',
    'singular_name'       => 'Meinung',
    'menu_name'           => 'Meinungen',
    'name_admin_bar'      => 'Meinung',
    'parent_item_colon'   => 'Parent Item:',
    'all_items'           => 'Alle Meinungen',
    'add_new_item'        => 'Add New Item',
    'add_new'             => 'Add New',
    'new_item'            => 'New Item',
    'edit_item'           => 'Edit Item',
    'update_item'         => 'Update Item',
    'view_item'           => 'View Item',
    'search_items'        => 'Search Item',
    'not_found'           => 'Not found',
    'not_found_in_trash'  => 'Not found in Trash',
  );
  $args = array(
    'label'               => 'Meinung',
    'description'         => 'Kundenmeinungen',
    'labels'              => $labels,
    'supports'            => array( 'title', 'editor', 'thumbnail', 'revisions', ),
    'hierarchical'        => false,
    'public'              => true,
    'show_ui'             => true,
    'show_in_menu'        => true,
    'menu_position'       => 5,
    'menu_icon'           => 'dashicons-thumbs-up',
    'show_in_admin_bar'   => true,
    'show_in_nav_menus'   => true,
    'can_export'          => true,
    'has_archive'         => true,
    'exclude_from_search' => true,
    'publicly_queryable'  => true,
    'capability_type'     => 'page',
  );
  register_post_type( 'testemonial', $args );

}
add_action( 'init', 'testemonial', 0 );

?>
