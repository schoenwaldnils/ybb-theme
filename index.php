<?php get_header(); ?>
  <div class="Area Area--banner">
    <div class="Banner">
      <?php
      if (has_post_thumbnail()) {
        the_post_thumbnail('banner');
      }
      else {
        echo '<img src="' . $url_image . '/layout/background-hero-default.jpg" alt="">';
      }
      ?>
    </div>
  </div>
  <div class="Area Area--main">
    <div class="Area-content u-cf">
      <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
          <?php the_content('weiterlesen..'); ?>
        <?php endwhile; ?>
      <?php endif; ?>
    </div>
  </div>
<?php get_footer(); ?>
