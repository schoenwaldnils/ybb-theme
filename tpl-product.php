<?php
/*
* Template Name: Produkt-tpl
*/
?>

<?php get_header(); ?>
  <div class="Area Area--banner">
    <div class="Banner Banner--product u-textCenter">
      <?php if (has_post_thumbnail()) { ?>
        <div class="Banner-product u-inlineBlock u-marginTop--large u-marginRight--large">
          <?php the_post_thumbnail('medium'); ?>
        </div>
      <?php } ?>
    </div>
  </div>

  <div class="Area Area--main">
    <div class="Area-content">
      <?php the_content(); ?>
    </div>
  </div>
  <?php if (get_the_block('Lila1-Headline') || get_the_block('Lila1-Box 1') || get_the_block('Lila1-Box 2') || get_the_block('Lila1-Box 3')) { ?>
    <div class="Area Area--main Area--purple">
      <div class="Area-content">
        <?php if (get_the_block('Lila1-Headline')) { ?>
          <h2 class="u-fontAlpha u-textCenter u-marginBottom--medium">
            <?php the_block( "Lila1-Headline", array('type' => 'one-liner')); ?>
          </h2>
        <?php } ?>
        <?php if (get_the_block('Lila1-Box 1') || get_the_block('Lila1-Box 2') || get_the_block('Lila1-Box 3')) { ?>
          <div class="Grid Grid--withGutter u-cf">
            <div class="Grid-cell u-sm-size1of3">
              <?php the_block( 'Lila1-Box 1'); ?>
            </div>
            <div class="Grid-cell u-sm-size1of3">
              <?php the_block( 'Lila1-Box 2'); ?>
            </div>
            <div class="Grid-cell u-sm-size1of3">
              <?php the_block( 'Lila1-Box 3'); ?>
            </div>
          </div>
        <?php } ?>
      </div>
    </div>
  <?php } ?>
  <?php if (get_the_block('Weiß-Headline') || get_the_block('Weiß-Links') || get_the_block('Weiß-Rechts')) { ?>
    <div class="Area Area--main">
      <div class="Area-content">
        <?php if (get_the_block('Weiß-Headline')) { ?>
          <h2 class="u-fontAlpha u-textCenter u-marginBottom--medium">
            <?php the_block( "Weiß-Headline", array('type' => 'one-liner')); ?>
          </h2>
        <?php } ?>
        <?php if (get_the_block('Weiß-Links') || get_the_block('Weiß-Rechts')) { ?>
          <div class="Grid Grid--withGutter u-cf">
            <div class="Grid-cell u-sm-size1of3">
              <?php the_block( 'Weiß-Links'); ?>
            </div>
            <div class="Grid-cell u-sm-size2of3">
              <?php the_block( 'Weiß-Rechts'); ?>
            </div>
          </div>
        <?php } ?>
      </div>
    </div>
  <?php } ?>
  <div class="Area Area--main Area--purple">
    <div class="Area-content">
      <?php if (get_the_block('Lila2-Headline')) { ?>
        <h2 class="u-fontAlpha u-textCenter u-marginBottom--medium">
          <?php the_block( "Lila2-Headline", array('type' => 'one-liner')); ?>
        </h2>
      <?php } ?>
      <?php if (get_the_block('Lila2-Links') || get_the_block('Lila2-Rechts')) { ?>
        <div class="Grid Grid--withGutter u-cf">
          <div class="Grid-cell u-sm-size2of3">
            <?php the_block( 'Lila2-Links'); ?>
          </div>
          <div class="Grid-cell u-sm-size1of3">
            <?php the_block( 'Lila2-Rechts'); ?>
          </div>
        </div>
      <?php } ?>
    </div>
  </div>

  <?php print component('partner-and-seal'); ?>

<?php get_footer(); ?>
