<?php
  $logo_style = "width: " . $ratio . "em; font-size: " . $height;
  $logo_p_style = "background-image: url( '" . $src . "');";
?>

<div class="Logo" style="<?php echo $logo_style; ?>">
  <p style="<?php echo $logo_p_style; ?>">
    <?php echo $title; ?>
  </p>
</div>
