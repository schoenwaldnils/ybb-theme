'use strict';

/**
 *@fileOverview
 *@version 0.0.1
 *
 * @namespace factorial.banner
 */


var $;

$ = require('jquery');
$.ui = require('jqueryui');

jQuery(document).ready(function($) {

  $.widget('factorial.banner', {

    /*
     * Options to be used as defaults.
     */

    options: {
      selectors: {
        'index': 0,
        'slider': '.ls-container',
        'navItem': '.Banner-menuItem',
        'textBlock': '.Banner-text'
      }
    },

    /**
     * Implements {@link https://api.jqueryui.com/jquery.widget/#method-_getCreateOptions|$.widget._getCreateOptions}
     *
     * @memberof factorial.banner
     * @instance
     * @private
     */

    _getCreateOptions: function() {
      var options = {};
      if (this.element.data('options')) {
        options = this.element.data('options');
      }
      return options;
    },

    /**
     * Implements {@link https://api.jqueryui.com/jquery.widget/#method-_create|$.widget._create}
     *
     * @memberof factorial.banner
     * @instance
     * @private
     */

    _create: function() {
      var elements = {};
      for (var key in this.options.selectors) {
        elements[key] = $(this.options.selectors[key]);
      }
      this._setOption('elements', elements);

      this._setOption('index', 0);

      this.elements.navItem.on( 'click', function(event) {
        var elem = event.currentTarget;
        $.proxy(this._preventEvents, this);

        this._setOption('index', $(elem).parent().index());
      }.bind(this));

    },

    /**
     * Implements {@link https://api.jqueryui.com/jquery.widget/#method-_destroy|$.widget._destroy}
     *
     * @memberof factorial.banner
     * @instance
     * @private
     */

    _destroy: function() {

    },

    /**
     * Implements {@link https://api.jqueryui.com/jquery.widget/#method-_setOption|$.widget._setOption}
     *
     * @memberof factorial.banner
     * @instance
     * @private
     */

    _setOption: function( key, value ) {
      switch (key) {
        case 'elements':
          this[key] = value;
          break;
        case 'index':
          console.log(value);
          this.options[key] = value;
          this._setSlide(value);
          break;
        default:
          console.log(value);
          this.options[key] = value;
          break;
      }
      this._super('_setOption', key, value);
    },

    _setSlide: function() {
      console.log('set_slide');

      $('#layerslider_1_1').layerSlider(this.options.index + 1);

      this.elements.navItem.removeClass('is-active');
      this.elements.textBlock.removeClass('is-active');

      this.elements.navItem
        .parent()
        .eq(this.options.index)
        .find(this.elements.navItem)
        .addClass('is-active');

      this.elements.textBlock
        .eq(this.options.index)
        .addClass('is-active');
    },

    _preventEvents: function(event) {
      event.preventDefault();
      event.stopPropagation();
      return false;
    }

  });

  $('.js-Banner').banner();

});
