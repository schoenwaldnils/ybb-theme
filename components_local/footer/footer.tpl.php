<footer class="Footer Grid l-textShadow">
  <div class="Grid-cell u-md-size1of2">
    <div class="Footer-content">
      <h3 class="u-textCenter u-marginBottom--medium">folgst du uns?</h3>
      <ul class="Footer-menu--social">
        <li class="u-inlineBlock">
          <a href="#" class="Icon Icon--instagram u-block">
            <span class="u-hiddenVisually">Instagramm Profil</span>
          </a>
        </li>
        <li class="u-inlineBlock">
          <a href="#" class="Icon Icon--facebook u-block">
            <span class="u-hiddenVisually">Facebook Profil</span>
          </a>
        </li>
        <li class="u-inlineBlock">
          <a href="#" class="Icon Icon--twitter u-block">
            <span class="u-hiddenVisually">Twitter Profil</span>
          </a>
        </li>
      </ul>
    </div>
  </div>
  <div class="Grid-cell u-md-size1of2">
    <div class="Footer-content">
      <h3 class="u-textCenter u-marginBottom--medium">jeden Monat liebe Worte...</h3>
      <?php dynamic_sidebar( "Footer right" ); ?>
    </div>
  </div>
  <div class="Grid-cell">
    <div class="Footer-content">
      <a class="Footer-logo u-inlineBlock l-imageRound u-marginBottom--small" href="<?php echo get_option('home'); ?>" title="<?php bloginfo( 'name' ); ?>">
        <?php
          $logo_settings = array(
            'ratio' => 1,
            'height' => '120px',
            'src' => $url_image . 'logo/yourbiobrands-logo.png',
            'title' => get_bloginfo( 'name' )
          );
          print component('logo', $logo_settings) ?>
      </a>
      <div class="u-marginBottom--medium u-fontDelta">from Hamburg with love <div class="Icon Icon--line-heart"></div></div>
      <div class="Footer-menu u-marginBottom--small">
        <?php dynamic_sidebar( "Footer bottom" ); ?>
      </div>
      <div class="Footer-meta">
        &copy; <?php echo date("Y") ?> yourbiobrands UG (Haftungsbeschränkt)
      </div>
    </div>
  </div>
</footer>
