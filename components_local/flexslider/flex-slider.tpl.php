<div class="FlexSlider js-FlexSlider" data-options="<?php json_encode($data_options) ?>">
  <ul class="FlexSlider-slides">
    <?php foreach ($items as $item) { ?>
      <li class="FlexSlider-slide">
        <?php print $item; ?>
      </li>
    <?php
    } ?>
  </ul>
</div>
