<div class="Area Area--main">
  <div class="Area-content">
    <div class="Grid">
      <div class="Grid-cell u-sm-size1of2">
        <h3 class="u-marginBottom--small">unsere Partner</h3>
        <div class="Partner">
          <a class="Partner-item u-marginRight--small">
            <?php
            $logo_settings = array(
              'ratio' => 1.768,
              'height' => '80px',
              'src' => $url_image . 'logo/partner-vegetarierbund.png',
              'title' => 'Vegetarierbund'
             );
            print component('logo', $logo_settings) ?>
          </a>
          <a class="Partner-item u-marginRight--small">
            <?php
            $logo_settings = array(
              'ratio' => 2.202,
              'height' => '80px',
              'src' => $url_image . 'logo/partner-tierschutz.png',
              'title' => 'Tierschutzbüro'
             );
            print component('logo', $logo_settings) ?>
          </a>
        </div>
      </div>
      <div class="Grid-cell u-sm-size1of2 u-textRight">
        <h3 class="u-marginBottom--small">natürlich biozertifiziert</h3>

        <?php
        $logo_bio = array(
          'ratio' => 1.19,
          'height' => '80px',
          'src' => $url_svg . 'bio-siegel.svg',
          'title' => 'Bio Siegel'
         );
        // print component('logo', $logo_bio)
        ?>

        <?php
        $logo_organic = array(
          'ratio' => 1.493,
          'height' => '80px',
          'src' => $url_svg . 'organic-logo.svg',
          'title' => 'Organic Logo'
         );
        print component('logo', $logo_organic) ?>
      </div>
    </div>
  </div>
</div>
