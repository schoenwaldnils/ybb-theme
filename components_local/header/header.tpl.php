<div class="Header u-posRelative">
  <div class="Arrange Arrange--middle">
    <div class="Arrange-sizeFit">
      <a class="Header-mainLogo u-block u-posAbsolute l-dropShadow l-imageRound" href="<?php echo get_option('home'); ?>" title="<?php bloginfo( 'name' ); ?>">
        <?php
          $logo_settings = array(
            'ratio' => 1,
            'height' => '1em',
            'src' => $url_image . 'logo/yourbiobrands-logo.png',
            'title' => get_bloginfo( 'name' )
          );
          print component('logo', $logo_settings) ?>
      </a>
    </div>
    <nav class="Arrange-sizeFill u-textRight">
      <?php wp_nav_menu( array(
        'container'=>false,
        'menu_class' => 'Menu Menu--header u-alignMiddle u-sm-fontNormal u-xs-fontDelta u-md-fontGamma',
        'theme_location' => 'header_menu',
        'walker' => new mainmanu_walker()
      )); ?>
      <div class="Header-wrapLogos u-hidden u-sm-inlineBlock u-marginLeft--small">
        <?php
          $logo_settings = array(
            'ratio' => 1.19,
            'height' => '42px',
            'src' => $url_svg . 'bio-siegel.svg',
            'title' => 'Bio Siegel'
           );
          //print component('logo', $logo_settings)
          ?>
        <?php
          $logo_settings = array(
            'ratio' => 1.493,
            'height' => '42px',
            'src' => $url_svg . 'organic-logo.svg',
            'title' => 'Organic Logo'
           );
          print component('logo', $logo_settings) ?>
      </div>
    </nav>
  </div>
</div>
