<?php
/*
 * Template Name: Boxen
 */
?>

<?php get_header(); ?>
  <div class="Area Area--banner">
    <div class="Banner Banner--slider js-Banner">
      <div class="Banner-sliderWrap">
        <?php layerslider('productslider') ?>
        <div class="Banner-menu u-posAbsolute">
          <div class="Banner-menuItems">
            <div class="Grid">
              <div class="Grid-cell u-size1of3">
                <div class="Banner-menuItem u-textCenter u-padding-v--small u-fontGamma">Vegane Wunder</div>
              </div>
              <div class="Grid-cell u-size1of3">
                <div class="Banner-menuItem u-textCenter u-padding-v--small u-fontGamma">Klassik Box</div>
              </div>
              <div class="Grid-cell u-size1of3">
                <div class="Banner-menuItem u-textCenter u-padding-v--small u-fontGamma">Glutenfreier Genuss</div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="Area Area--main">
        <div class="Area-content">
          <div class="Banner-text">
            <?php the_block( 'Box1'); ?>
          </div>
          <div class="Banner-text">
            <?php the_block( 'Box2'); ?>
          </div>
          <div class="Banner-text">
            <?php the_block( 'Box3'); ?>
          </div>
        </div>
      </div>
    </div>
  </div>

  <?php if (get_the_content()) { ?>
    <div class="Area Area--main">
      <div class="Area-content">
        <?php the_content(); ?>
      </div>
    </div>
  <?php } ?>

  <div class="Area Area--main Area--purple">
    <div class="Area-content">
      <h2 class="u-fontAlpha u-marginBottom--medium">
        <?php the_block( "Lila-Headline", array('type' => 'one-liner')); ?>
      </h2>
      <?php the_block( 'Lila-Rechts'); ?>
    </div>
  </div>

  <?php print component('partner-and-seal') ?>

<?php get_footer(); ?>
